<?php

Auth::routes();

Route::get('/', 'CarController@index')->name('home');

Route::resource('cars', 'CarController');

Route::group(['prefix' => 'car'], function(){
  Route::group(['prefix' => '{car}'], function($car){

    // Files
    Route::resource('tenencies', 'TenencyController');
    Route::resource('fines', 'FineController');
    Route::resource('services', 'ServiceController');
    Route::resource('documents', 'DocumentController');

  });
});
