<?php

use Faker\Generator as Faker;

$factory->define(App\Car::class, function (Faker $faker) {
    return [
        'plate' => strtoupper($faker->lexify('???')) . $faker->numberBetween(100,999),
        'model' => $faker->numberBetween(2010, 2019),
        'brand' => $faker->randomElement(['FORD', 'HONDA', 'CHEVROLET', 'NISSAN']),
        'color' => $faker->randomElement(['black', 'white', 'red', 'blue']),
        'image' => 'car-default.jpg',
        'user_id' => 1,
    ];
});
