<div class="alert alert-warning alert-dismissible fade show" role="alert">
	<i class="fas fa-info-circle"></i> {{ session('msg') }}
 	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</button>
</div>