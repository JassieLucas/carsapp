{!! Form::open(['route' => ['services.store', $car], 'files' => true]) !!}

	<div class="form-group row">
		<div class="col-md-6">
			<label for="date">Fecha del servicio:</label>
			{{ Form::date('date', date('Y-m-d'), ['class' => 'form-control']) }}
		</div>
		<div class="col-md-6">
			<label for="amount">Costo:</label>
			{{ Form::text('amount', null, ['placeholder' => 'Cantidad a pagar...', 'class' => 'form-control']) }}
		</div>
	</div>

	<div class="form-group row">
		<div class="col-md-12">
			<label for="kilometers">Kilometros:</label>
			{{ Form::text('kilometers', null, ['placeholder' => 'Ingresa el kilometraje...', 'class' => 'form-control']) }}
		</div>
	</div>

	<div class="form-group row">
		<div class="col-md-6">
			<label for="workshop">Taller:</label>
			{{ Form::text('workshop', null, ['placeholder' => 'Ingresa el taller...', 'class' => 'form-control']) }}
		</div>
		<div class="col-md-6">
			<label for="type">Tipo:</label>
			{{ Form::text('type', null, ['placeholder' => 'Ingresa el tipo de servicio...', 'class' => 'form-control']) }}
		</div>
	</div>

	

	<div class="form-group">
			<label for="file">Archivo:</label>
			{{ Form::file('file') }}
	</div><hr>

	<div class="form-group text-center">
			<a href="{{ route('services.index', $car) }}" class="btn btn-secondary">
				Regresar
			</a>
			<button type="submit" class="btn btn-primary">
				Guardar <i class="fas fa-save"></i>
			</button>
	</div>

{!! Form::close() !!}