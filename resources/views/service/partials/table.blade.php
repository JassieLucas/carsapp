<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>#</th>
            <th>Fecha</th>
            <th>Taller</th>
            <th>Tipo</th>
            <th>Monto</th>
            <th>Kilometros</th>
            <th>Registrada</th>
            <th>Actualizada</th>
            <th>Archivo</th>
            <th>Acciones</th>
        </tr>
    </thead>

    @forelse($services as $service)
        <tr>
            <td>{{ $loop->index + 1 }}</td>
            <td>{{ \Carbon\Carbon::parse($service->date)->format('d/m/Y') }}</td>
            <td>{{ $service->workshop }}</td>
            <td>{{ $service->type }}</td> 
            <td>$ {{ number_format($service->amount, 2) }}</td>
            <td>{{ $service->kilometers}}</td>
            <td>{{ $service->created_at->format('d/m/Y') }}</td>
            <td>{{ $service->updated_at->format('d/m/Y') }}</td>
            <td>
                @if($service->file)
                    <a href="{{ asset("storage/services/" . $service->file) }}" target="_blank" class="btn btn-sm btn-info">{{ $service->file }}</a>
                @endif
            </td>
            <td>
                <a href="{{ route('services.edit', [$car, $service]) }}" class="btn btn-sm btn-success">
                    <i class="fas fa-pencil-alt"></i>
                </a>
                <a class="btn btn-sm btn-danger" href="#"
                   onclick="event.preventDefault();
                                 document.getElementById('delete-{{ $service->id }}').submit();">
                    <i class="fas fa-trash-alt"></i>
                </a>

                <form id="delete-{{ $service->id }}" action="{{ route('services.destroy', [$car, $service]) }}" method="POST" style="display: none;">
                    @csrf
                    <input type="hidden" name="_method" value="DELETE">
                </form></td>
        </tr>
    @empty
        <tr>
            <td colspan="9">
                <p class="alert alert-info">No tienes servicios</p>
            </td>

        </tr>
    @endforelse
</table>