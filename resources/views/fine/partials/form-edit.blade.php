{!! Form::open(['route' => ['fines.update', $car, $fine], 'files' => true]) !!}

	<input type="hidden" name="_method" value="PUT">

	<div class="form-group row">
		<div class="col-md-12">
			<label for="reason">Motivo:</label>
			{{ Form::text('reason', $fine->reason, ['placeholder' => 'Ingresa el motivo...', 'class' => 'form-control']) }}
		</div>
		<div class="col-md-6">
			<label for="finedate">Fecha de multa:</label>
			{{ Form::date('finedate', $fine->finedate->format('Y-m-d'), ['class' => 'form-control']) }}
		</div>
		<div class="col-md-6">
			<label for="paiddate">Fecha de pago:</label>
			{{ Form::date('paiddate', $fine->paiddate->format('Y-m-d'), ['class' => 'form-control']) }}
		</div>
	</div>

	<div class="form-group row">
		<div class="col-md-6">
			<label for="amount">Cantidad a pagar:</label>
			{{ Form::text('amount', $fine->amount, ['placeholder' => 'Cantidad a pagar...', 'class' => 'form-control']) }}
		</div>
		<div class="col-md-6">
			<label>Ya fue pagada?: </label>
			<div class="custom-control custom-switch">
				<input type="checkbox" name="paid" class="custom-control-input" id="check-paid" checked="{{ $fine->paid }}">
				<label class="custom-control-label" for="check-paid">Pagada:</label>
			</div>
		</div>
	</div>

	

	<div class="form-group">
		@if($fine->file)
            <a href="{{ asset("storage/fines/" . $fine->file) }}" target="_blank" class="btn btn-sm btn-info">{{ $fine->file }}</a>
        @endif
			<label for="file">Archivo:</label>
			{{ Form::file('file') }}
	</div><hr>

	<div class="form-group text-center">
			<a href="{{ route('fines.index', $car) }}" class="btn btn-secondary">
				Regresar
			</a>
			<button type="submit" class="btn btn-primary">
				Actualizar <i class="fas fa-save"></i>
			</button>
	</div>

{!! Form::close() !!}