{!! Form::open(['route' => ['fines.store', $car], 'files' => true]) !!}

	<div class="form-group row">
		<div class="col-md-12">
			<label for="reason">Motivo:</label>
			{{ Form::text('reason', null, ['placeholder' => 'Ingresa el motivo...', 'class' => 'form-control']) }}
		</div>
		<div class="col-md-6">
			<label for="finedate">Fecha de multa:</label>
			{{ Form::date('finedate', date('Y-m-d'), ['class' => 'form-control']) }}
		</div>
		<div class="col-md-6">
			<label for="paiddate">Fecha de pago:</label>
			{{ Form::date('paiddate', null, ['class' => 'form-control']) }}
		</div>
	</div>

	<div class="form-group row">
		<div class="col-md-6">
			<label for="amount">Cantidad a pagar:</label>
			{{ Form::text('amount', null, ['placeholder' => 'Cantidad a pagar...', 'class' => 'form-control']) }}
		</div>
		<div class="col-md-6">
			<label>Ya fue pagada?: </label>
			<div class="custom-control custom-switch">
				<label class="custom-control-label" for="check-paid">Pagada:</label>
				<input type="checkbox" name="paid" class="custom-control-input" id="check-paid">
			</div>
		</div>
	</div>

	

	<div class="form-group">
			<label for="file">Archivo:</label>
			{{ Form::file('file') }}
	</div><hr>

	<div class="form-group text-center">
			<a href="{{ route('fines.index', $car) }}" class="btn btn-secondary">
				Regresar
			</a>
			<button type="submit" class="btn btn-primary">
				Guardar <i class="fas fa-save"></i>
			</button>
	</div>

{!! Form::close() !!}