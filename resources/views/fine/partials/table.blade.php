<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>#</th>
            <th>Motivo</th>
            <th>Fecha de multa</th>
            <th>Fecha de pago</th>
            <th>Monto</th>
            <th>Pagada</th>
            <th>Registrada</th>
            <th>Actualizada</th>
            <th>Archivo</th>
            <th>Acciones</th>
        </tr>
    </thead>

    @forelse($fines as $fine)
        <tr>
            <td>{{ $loop->index + 1 }}</td>
            <td>{{ $fine->reason }}</td>
            <td>{{ \Carbon\Carbon::parse($fine->finedate)->format('d/m/Y') }}</td>
            <td>
                @if($fine->paiddate)
                {{ \Carbon\Carbon::parse($fine->paiddate)->format('d/m/Y') }}
                @endif
            </td>
            <td>$ {{ number_format($fine->amount, 2) }}</td>
            <td>{{ $fine->paid ? 'Si' : 'No' }}</td>
            <td>{{ $fine->created_at->format('d/m/Y') }}</td>
            <td>{{ $fine->updated_at->format('d/m/Y') }}</td>
            <td>
                @if($fine->file)
                    <a href="{{ asset("storage/fines/" . $fine->file) }}" target="_blank" class="btn btn-sm btn-info">{{ $fine->file }}</a>
                @endif
            </td>
            <td>
                <a href="{{ route('fines.edit', [$car, $fine]) }}" class="btn btn-sm btn-success">
                    <i class="fas fa-pencil-alt"></i>
                </a>
                <a class="btn btn-sm btn-danger" href="#"
                   onclick="event.preventDefault();
                                 document.getElementById('delete-{{ $fine->id }}').submit();">
                    <i class="fas fa-trash-alt"></i>
                </a>

                <form id="delete-{{ $fine->id }}" action="{{ route('fines.destroy', [$car, $fine]) }}" method="POST" style="display: none;">
                    @csrf
                    <input type="hidden" name="_method" value="DELETE">
                </form></td>
        </tr>
    @empty
        <tr>
            <td colspan="9">
                <p class="alert alert-info">No tienes Multas</p>
            </td>

        </tr>
    @endforelse
</table>