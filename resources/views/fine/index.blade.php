@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
        <h3><i class="fas fa-id-card fa-2x"></i> Multas</h3>
            <div class="card">
                <div class="card-header">
                    <h3>Multas de {{ $car->brand }} <span class="badge badge-light">{{ $car->plate }}</span>
                    <a href="{{ route('fines.create', $car) }}" class="btn btn-warning float-right">
                    + Agregar
                    </a> </h3>
                </div>

                <div class="card-body">
                    @include('fine.partials.table')
                    </div>

                    <div class="card-footer text-center">
                        <a href="{{ route('cars.show', $car) }}" class="btn btn-secondary">
                            <i class="fas fa-chevron-circle-left"></i> Regresar
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
