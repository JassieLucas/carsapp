<div class="list-group">
	  <a href="{{ route('tenencies.index', $car) }}" class="list-group-item list-group-item-action list-group-item-dark">
	    <i class="fas fa-id-card fa-2x"></i> Tenencias
	  </a>
	  <a href="{{ route('fines.index', $car) }}" class="list-group-item list-group-item-action list-group-item-dark">
	  	<i class="fas fa-file-invoice-dollar fa-2x"></i> Multas
	  </a>
	  <a href="#" class="list-group-item list-group-item-action list-group-item-dark">
	  	<i class="fas fa-tools fa-2x"></i> Verificaciones
	  </a>
	  <a href="{{ route('services.index', $car) }}" class="list-group-item list-group-item-action list-group-item-dark">
	  	<i class="fas fa-wrench fa-2x"></i> Servicios
	  </a>
	  <a href="{{ route('documents.index', $car) }}" class="list-group-item list-group-item-action list-group-item-dark">
	  	<i class="fas fa-file-alt fa-2x"></i> Documentos
	  </a>
</div>