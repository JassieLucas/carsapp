{!! Form::open(['route' => ['cars.update', $car], 'files' => true]) !!}

	<input type="hidden" name="_method" value="PUT">

	<div class="form-group">
			<label for="plate">Placas:</label>
			{{ Form::text('plate', $car->plate, ['placeholder' => 'Ingresa las placas...', 'class' => 'form-control']) }}
	</div>

	<div class="form-group">
			<label for="model">Model:</label>
			{{ Form::number('model', $car->model, ['min' => 2000, 'max' => 2050, 'class' => 'form-control']) }}
	</div>

	<div class="form-group">
			<label for="brand">Marca:</label>
			{{ Form::text('brand', $car->brand, ['placeholder' => 'Ingresa la marca...', 'class' => 'form-control']) }}
	</div>

	<div class="form-group">
			<label for="color">Color:</label>
			<input type="color" value="{{ $car->color }}" name="color" class="form-control">
	</div>

	<div class="form-group">
			<label for="image">Imagen:</label>
			<img src="{{ asset("storage/cars/" . $car->image) }}">
			{{ Form::file('image') }}
	</div><hr>

	<div class="form-group text-center">
			<a href="{{ route('home') }}" class="btn btn-secondary">
				Regresar
			</a>
			<button type="submit" class="btn btn.primary">
				Actualizar
			</button>
	</div>

{!! Form::close() !!}