@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4">
            @include('car.nav')
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h4>{{ $car->brand }}</h4>
                </div>

                <div class="card-body">
                    <div class="card bg-dark text-white">
                        <img class="card-img-top" src="{{ asset("storage/cars/" . $car->image) }}" alt="Card image cap">
                        <div class="card-body">
                            <p><b>Placas:</b> {{ $car->plate }}</p>
                            <p><b>Marca:</b> {{ $car->brand }}</p>
                            <p><b>Modelo:</b> {{ $car->model }}</p>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
