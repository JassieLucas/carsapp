@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                	Cars App [Nuevo Coche]
                	@include('partials.errors')
                </div>

                <div class="card-body">
                	@include('car.form-create')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection