@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    Cars App 
                    <a href="{{ route('cars.create') }}" class="btn btn-warning">
                    + Car
                    </a> 
                </div>

                <div class="card-body">
                    <div class="card-columns">
                        @forelse($cars as $car)
                            <div class="card bg-dark text-white">
                                <img class="card-img-top" src="{{ asset("storage/cars/" . $car->image) }}" alt="Card image cap">
                                <div class="card-body">
                                    <p><b>Placas:</b> {{ $car->plate }}</p>
                                    <p><b>Marca:</b> {{ $car->brand }}</p>
                                    <p><b>Modelo:</b> {{ $car->model }}</p>
                                </div>
                                <div class="card-footer text-center">
                                    
                                    <a href="{{ route('cars.show', $car) }}" class="btn btn-info">
                                        <i class="fas fa-eye"></i>
                                    </a>

                                    <a href="{{ route('cars.edit', $car) }}" class="btn btn-warning">
                                        <i class="fas fa-pencil-alt"></i>
                                    </a>

                                    <a class="btn btn-danger" href="#"
                                       onclick="event.preventDefault();
                                                     document.getElementById('delete-{{ $car->id }}').submit();">
                                        <i class="fas fa-trash-alt"></i>
                                    </a>

                                    <form id="delete-{{ $car->id }}" action="{{ route('cars.destroy', $car) }}" method="POST" style="display: none;">
                                        @csrf
                                        <input type="hidden" name="_method" value="DELETE">
                                    </form>
                                </div>
                            </div>
                        @empty
                            No tienes coches
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
