{!! Form::open(['route' => 'cars.store', 'files' => true]) !!}

	<div class="form-group">
			<label for="plate">Placas:</label>
			{{ Form::text('plate', null, ['placeholder' => 'Ingresa las placas...', 'class' => 'form-control']) }}
	</div>

	<div class="form-group">
			<label for="model">Model:</label>
			{{ Form::number('model', date('Y'), ['min' => 2000, 'max' => 2050, 'class' => 'form-control']) }}
	</div>

	<div class="form-group">
			<label for="brand">Marca:</label>
			{{ Form::text('brand', null, ['placeholder' => 'Ingresa la marca...', 'class' => 'form-control']) }}
	</div>

	<div class="form-group">
			<label for="color">Color:</label>
			<input type="color" name="color" class="form-control">
	</div>

	<div class="form-group">
			<label for="image">Imagen:</label>
			{{ Form::file('image') }}
	</div><hr>

	<div class="form-group text-center">
			<a href="{{ route('home') }}" class="btn btn-secondary">
				Regresar
			</a>
			<button type="submit" class="btn btn.primary">
				Guardar
			</button>
	</div>

{!! Form::close() !!}