{!! Form::open(['route' => ['tenencies.update', $car, $tenency], 'files' => true]) !!}

	<input type="hidden" name="_method" value="PUT">

	<div class="form-group row">
		<div class="col-md-6">
			<label for="year">Año:</label>
			{{ Form::number('year', $tenency->year, ['min' => date('Y') - 5, 'max' => date('Y') + 1,'class' => 'form-control']) }}
		</div>
		<div class="col-md-6">
			<label for="date">Fecha:</label>
			{{ Form::date('date', $tenency->date->format('Y-m-d'), ['class' => 'form-control']) }}
		</div>
	</div>

	<div class="form-group row">
		<div class="col-md-6">
			<label for="amount">Cantidad a pagar:</label>
			{{ Form::text('amount', $tenency->amount, ['placeholder' => 'Cantidad a pagar...', 'class' => 'form-control']) }}
		</div>
		<div class="col-md-6">
			<label>Cantidad a pagar: </label>
			<div class="custom-control custom-switch">
				<input type="checkbox" name="paid" class="custom-control-input" id="check-paid" checked="{{ $tenency->paid }}">
				<label class="custom-control-label" for="check-paid">Pagada:</label>
			</div>
		</div>
	</div>

	

	<div class="form-group">
		@if($tenency->file)
            <a href="{{ asset("storage/tenencies/" . $tenency->file) }}" target="_blank" class="btn btn-sm btn-info">{{ $tenency->file }}</a>
        @endif
			<label for="file">Archivo:</label>
			{{ Form::file('file') }}
	</div><hr>

	<div class="form-group text-center">
			<a href="{{ route('tenencies.index', $car) }}" class="btn btn-secondary">
				Regresar
			</a>
			<button type="submit" class="btn btn-primary">
				Actualizar <i class="fas fa-save"></i>
			</button>
	</div>

{!! Form::close() !!}