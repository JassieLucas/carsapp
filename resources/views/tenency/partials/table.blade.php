<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>#</th>
            <th>Año</th>
            <th>Fecha</th>
            <th>Monto</th>
            <th>Pagada</th>
            <th>Registrada</th>
            <th>Actualizada</th>
            <th>Archivo</th>
            <th>Acciones</th>
        </tr>
    </thead>

    @forelse($tenencies as $tenency)
        <tr>
            <td>{{ $loop->index + 1 }}</td>
            <td>{{ $tenency->year }}</td>
            <td>{{ \Carbon\Carbon::parse($tenency->date)->format('d/m/Y') }}</td>
            <td>$ {{ number_format($tenency->amount, 2) }}</td>
            <td>{{ $tenency->paid ? 'Si' : 'No' }}</td>
            <td>{{ $tenency->created_at->format('d/m/Y') }}</td>
            <td>{{ $tenency->updated_at->format('d/m/Y') }}</td>
            <td>
                @if($tenency->file)
                    <a href="{{ asset("storage/tenencies/" . $tenency->file) }}" target="_blank" class="btn btn-sm btn-info">{{ $tenency->file }}</a>
                @endif
            </td>
            <td>
                <a href="{{ route('tenencies.edit', [$car, $tenency]) }}" class="btn btn-sm btn-success">
                    <i class="fas fa-pencil-alt"></i>
                </a>
                <a class="btn btn-sm btn-danger" href="#"
                   onclick="event.preventDefault();
                                 document.getElementById('delete-{{ $tenency->id }}').submit();">
                    <i class="fas fa-trash-alt"></i>
                </a>

                <form id="delete-{{ $tenency->id }}" action="{{ route('tenencies.destroy', [$car, $tenency]) }}" method="POST" style="display: none;">
                    @csrf
                    <input type="hidden" name="_method" value="DELETE">
                </form></td>
        </tr>
    @empty
        <tr>
            <td colspan="9">
                <p class="alert alert-info">No tienes tenencias</p>
            </td>

        </tr>
    @endforelse
</table>