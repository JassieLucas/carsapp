{!! Form::open(['route' => ['documents.update', $car, $document], 'files' => true]) !!}

	<input type="hidden" name="_method" value="PUT">

	<div class="form-group row">
		<div class="col-md-6">
			<label for="title">Titulo:</label>
			{{ Form::text('title', $document->title, ['placeholder' => 'Ingresa el titulo...', 'class' => 'form-control']) }}
		</div>
		<div class="col-md-6">
			<label for="reference">Referencia:</label>
			{{ Form::text('reference', $document->reference, ['placeholder' => 'Ingresa la referencia...', 'class' => 'form-control']) }}
		</div>
		<div class="col-md-12">
			<label for="description">Descripcion:</label>
			{{ Form::textarea('description', $document->description, ['placeholder' => 'Ingresa el la description...', 'class' => 'form-control']) }}
		</div>
	</div>

	<div class="form-group">
		@if($document->file)
            <a href="{{ asset("storage/documents/" . $document->file) }}" target="_blank" class="btn btn-sm btn-info">{{ $document->file }}</a>
        @endif
			<label for="file">Archivo:</label>
			{{ Form::file('file') }}
	</div><hr>

	<div class="form-group text-center">
			<a href="{{ route('documents.index', $car) }}" class="btn btn-secondary">
				Regresar
			</a>
			<button type="submit" class="btn btn-primary">
				Actualizar <i class="fas fa-save"></i>
			</button>
	</div>

{!! Form::close() !!}