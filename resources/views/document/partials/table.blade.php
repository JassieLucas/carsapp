<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>#</th>
            <th>Titulo</th>
            <th>Descripcion</th>
            <th>Referencia</th>
            <th>Registrada</th>
            <th>Actualizada</th>
            <th>Archivo</th>
            <th>Acciones</th>
        </tr>
    </thead>

    @forelse($documents as $document)
        <tr>
            <td>{{ $loop->index + 1 }}</td>
            <td>{{ $document->title }}</td>
            <td>{{ $document->description }}</td> 
            <td>{{ $document->reference }}</td>
            <td>{{ $document->created_at->format('d/m/Y') }}</td>
            <td>{{ $document->updated_at->format('d/m/Y') }}</td>
            <td>
                @if($document->file)
                    <a href="{{ asset("storage/documents/" . $document->file) }}" target="_blank" class="btn btn-sm btn-info">{{ $document->file }}</a>
                @endif
            </td>
            <td class="text-nowrap">
                <a href="{{ route('documents.edit', [$car, $document]) }}" class="btn btn-sm btn-success">
                    <i class="fas fa-pencil-alt"></i>
                </a>
                <a class="btn btn-sm btn-danger" href="#"
                   onclick="event.preventDefault();
                                 document.getElementById('delete-{{ $document->id }}').submit();">
                    <i class="fas fa-trash-alt"></i>
                </a>

                <form id="delete-{{ $document->id }}" action="{{ route('documents.destroy', [$car, $document]) }}" method="POST" style="display: none;">
                    @csrf
                    <input type="hidden" name="_method" value="DELETE">
                </form></td>
        </tr>
    @empty
        <tr>
            <td colspan="9">
                <p class="alert alert-info">No tienes Documentos</p>
            </td>

        </tr>
    @endforelse
</table>