{!! Form::open(['route' => ['documents.store', $car], 'files' => true]) !!}

	<div class="form-group row">
		<div class="col-md-6">
			<label for="title">Titulo:</label>
			{{ Form::text('title', null, ['placeholder' => 'Ingresa el titulo...', 'class' => 'form-control']) }}
		</div>
		<div class="col-md-6">
			<label for="reference">Referencia:</label>
			{{ Form::text('reference', null, ['placeholder' => 'Ingresa la referencia...', 'class' => 'form-control']) }}
		</div>
		<div class="col-md-12">
			<label for="description">Descripcion:</label>
			{{ Form::textarea('description', null, ['placeholder' => 'Ingresa el la description...', 'class' => 'form-control']) }}
		</div>
	</div>

	<div class="form-group">
			<label for="file">Archivo:</label>
			{{ Form::file('file') }}
	</div><hr>

	<div class="form-group text-center">
			<a href="{{ route('documents.index', $car) }}" class="btn btn-secondary">
				Regresar
			</a>
			<button type="submit" class="btn btn-primary">
				Guardar <i class="fas fa-save"></i>
			</button>
	</div>

{!! Form::close() !!}