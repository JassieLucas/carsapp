@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
        <h3><i class="fas fa-id-card fa-2x"></i> Documentos <small>Editar documentos</small></h3>
            <div class="card">
                <div class="card-header">
                    @include('partials.errors')
                </div>

                <div class="card-body">
                    @include('document.partials.form-edit')
                </div>

                
            </div>
        </div>
    </div>
</div>
@endsection
