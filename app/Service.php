<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $dates = [
		'date',
	];
    public function car()
    {
    	return $this->belongsTo(Car::class);
    }
}
