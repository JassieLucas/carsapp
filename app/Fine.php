<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fine extends Model
{
    protected $dates = [
		'finedate',
		'paiddate',
	];
    public function car()
    {
    	return $this->belongsTo(Car::class);
    }
}
