<?php

namespace App\Http\Controllers;

use App\Document;
use Illuminate\Http\Request;
use App\Car;
use Illuminate\Support\Facades\Storage;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Car $car)
    {
        $documents = $car->documents();
        return view('document.index', compact('documents', 'car'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Car $car)
    {
        return view('document.create', compact('car'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Car $car)
    {
        $request->validate([
            'title' => 'required',
            'file' => 'required|file|max:2048',
        ]);

        $document = new document;
        $document->title = $request->title;
        $document->description = $request->description;
        $document->reference = $request->reference;
        $document->car_id = $car->id;

        if($request->has('file')){
            $file = $request->file('file');
            $filename = 'document-' . time() . '.' . $file->getClientOriginalExtension();
            $file->storeAs("public/documents/", $filename);

            $document->file = $filename;
        }

        if ($document->save()) {
            return redirect()
                ->route('documents.index', $car)
                ->with('msg', 'Documento registrado satisfactoriamente');
        }

        return redirect()
                ->route('documents.index', $car)
                ->withInput()
                ->with('msg', 'El documento NO pudo ser registrado');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function show(Document $document)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function edit(Car $car,Document $document)
    {
        return view('document.edit', compact('car', 'document'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Car $car, Document $document)
    {
        $request->validate([
            'title' => 'required',
            'file' => 'file|max:2048',
        ]);

        $document->title = $request->title;
        $document->description = $request->description;
        $document->reference = $request->reference;

        if($request->has('file')){
            Storage::delete('public/documents/' . $document->file);

            $file = $request->file('file');
            $filename = 'document-' . time() . '.' . $file->getClientOriginalExtension();
            $file->storeAs("public/documents/", $filename);

            $document->file = $filename;
        }

        if ($document->save()) {
            return redirect()
                ->route('documents.index', $car)
                ->with('msg', 'Documento actualizado satisfactoriamente');
        }

        return redirect()
                ->route('documents.index', $car)
                ->withInput()
                ->with('msg', 'El documento NO pudo ser actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function destroy(Car $car,Document $document)
    {
        Storage::delete("public/documents/" . $document->file);

        if($document->delete()){
            return redirect()
                ->route('documents.index', $car)
                ->with('msg', 'Documento eliminado correctamente');
        }

        return redirect()
                ->route('documents.index', $car)
                ->with('msg', 'El documento NO se pudo eliminar');
    }
}
