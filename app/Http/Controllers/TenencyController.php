<?php

namespace App\Http\Controllers;

use App\Tenency;
use Illuminate\Http\Request;
use App\Car;
use Illuminate\Support\Facades\Storage;

class TenencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Car $car)
    {
        $tenencies = $car->tenencies();
        return view('tenency.index', compact('tenencies', 'car'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Car $car)
    {
        return view('tenency.create', compact('car'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Car $car)
    {
        $request->validate([
            'year' => 'required|integer',
            'date' => 'required|date',
            'amount' => 'required|numeric',

        ]);

        $tenency = new Tenency;
        $tenency->year = $request->year;
        $tenency->date = $request->date;
        $tenency->amount = $request->amount;
        $tenency->paid = $request->has('paid');
        $tenency->car_id = $car->id;

        if($request->has('file')){
            $file = $request->file('file');
            $filename = 'tenency-' . time() . '.' . $file->getClientOriginalExtension();
            $file->storeAs("public/tenencies/", $filename);

            $tenency->file = $filename;
        }

        if ($tenency->save()) {
            return redirect()
                ->route('tenencies.index', $car)
                ->with('msg', 'Tenencia registrada satisfactoriamente');
        }

        return redirect()
                ->route('tenencies.index', $car)
                ->withInput()
                ->with('msg', 'La tenencia NO pudo ser registrada');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tenency  $tenency
     * @return \Illuminate\Http\Response
     */
    public function show(Tenency $tenency)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tenency  $tenency
     * @return \Illuminate\Http\Response
     */
    public function edit(Car $car,Tenency $tenency)
    {
        return view('tenency.edit', compact('car', 'tenency'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tenency  $tenency
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Car $car, Tenency $tenency)
    {
        $request->validate([
            'year' => 'required|integer',
            'date' => 'required|date',
            'amount' => 'required|numeric',

        ]);

        $tenency->year = $request->year;
        $tenency->date = $request->date;
        $tenency->amount = $request->amount;
        $tenency->paid = $request->has('paid');

        if($request->has('file')){
            Storage::delete('public/tenencies/' . $tenency->file);

            $file = $request->file('file');
            $filename = 'tenency-' . time() . '.' . $file->getClientOriginalExtension();
            $file->storeAs("public/tenencies/", $filename);

            $tenency->file = $filename;
        }

        if ($tenency->save()) {
            return redirect()
                ->route('tenencies.index', $car)
                ->with('msg', 'Tenencia actualizada satisfactoriamente');
        }

        return redirect()
                ->route('tenencies.edit', $car)
                ->withInput()
                ->with('msg', 'La tenencia NO pudo ser actualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tenency  $tenency
     * @return \Illuminate\Http\Response
     */
    public function destroy(Car $car, Tenency $tenency)
    {
        Storage::delete("public/tenencies/" . $tenency->file);

        if($tenency->delete()){
            return redirect()
                ->route('tenencies.index', $car)
                ->with('msg', 'Tenencia eliminada correctamente');
        }

        return redirect()
                ->route('tenencies.index', $car)
                ->with('msg', 'La tenencia NO se pudo eliminar');
    }
}
