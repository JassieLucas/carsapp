<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$cars = auth()->user()->cars;
        return view('home.index', compact('cars'));
    }

    public function create()
    {
    	return view('home.create');
    }
}
