<?php

namespace App\Http\Controllers;

use App\Fine;
use Illuminate\Http\Request;
use App\Car;
use Illuminate\Support\Facades\Storage;

class FineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Car $car)
    {
        $fines = $car->fines();
        return view('fine.index', compact('fines', 'car'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Car $car)
    {
        return view('fine.create', compact('car'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Car $car)
    {
        $request->validate([
            'finedate' => 'required',
            'amount' => 'required|numeric',
            'reason' => 'required',

        ]);

        $fine = new Fine;
        $fine->finedate = $request->finedate;
        $fine->paiddate = $request->paiddate;
        $fine->amount = $request->amount;
        $fine->reason = $request->reason;
        $fine->paid = $request->has('paid');
        $fine->car_id = $car->id;

        if($request->has('file')){
            $file = $request->file('file');
            $filename = 'fine-' . time() . '.' . $file->getClientOriginalExtension();
            $file->storeAs("public/fines/", $filename);

            $fine->file = $filename;
        }

        if ($fine->save()) {
            return redirect()
                ->route('fines.index', $car)
                ->with('msg', 'Multa registrada satisfactoriamente');
        }

        return redirect()
                ->route('fines.index', $car)
                ->withInput()
                ->with('msg', 'La Multa NO pudo ser registrada');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Fine  $fine
     * @return \Illuminate\Http\Response
     */
    public function show(Fine $fine)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Fine  $fine
     * @return \Illuminate\Http\Response
     */
    public function edit(Car $car,Fine $fine)
    {
        return view('fine.edit', compact('car', 'fine'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Fine  $fine
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Car $car, Fine $fine)
    {
        $request->validate([
            'finedate' => 'required',
            'amount' => 'required|numeric',
            'reason' => 'required',

        ]);

        $fine->finedate = $request->finedate;
        $fine->paiddate = $request->paiddate;
        $fine->amount = $request->amount;
        $fine->reason = $request->reason;
        $fine->paid = $request->has('paid');

        if($request->has('file')){
            Storage::delete('public/fines/' . $fine->file);

            $file = $request->file('file');
            $filename = 'fine-' . time() . '.' . $file->getClientOriginalExtension();
            $file->storeAs("public/fines/", $filename);

            $fine->file = $filename;
        }

        if ($fine->save()) {
            return redirect()
                ->route('fines.index', $car)
                ->with('msg', 'Multa registrada satisfactoriamente');
        }

        return redirect()
                ->route('fines.index', $car)
                ->withInput()
                ->with('msg', 'La Multa NO pudo ser registrada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Fine  $fine
     * @return \Illuminate\Http\Response
     */
    public function destroy(Car $car, Fine $fine)
    {
        Storage::delete("public/fines/" . $fine->file);

        if($fine->delete()){
            return redirect()
                ->route('fines.index', $car)
                ->with('msg', 'Multa eliminada correctamente');
        }

        return redirect()
                ->route('fines.index', $car)
                ->with('msg', 'La Multa NO se pudo eliminar');
    }
}
