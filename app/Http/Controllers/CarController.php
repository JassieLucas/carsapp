<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Car;
use Illuminate\Support\Facades\Storage;

class CarController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $cars = auth()->user()->cars;
        return view('car.index', compact('cars'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('car.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'plate' => 'required|unique:cars',
            'model' => 'required|integer',
            'brand' => 'required',
            'color' => 'required',
            'image' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:2048',
        ]);

        $file = $request->file('image');
        $filename = 'car-' . time() . '.' . $file->getClientOriginalExtension();
        $file->storeAs("public/cars/", $filename);

        $car = new Car;
        $car->plate = $request->plate;
        $car->model = $request->model;
        $car->brand = $request->brand;
        $car->color = $request->color;
        $car->image = $filename;
        $car->user_id = auth()->user()->id;

        if($car->save()){
            return redirect()
                ->route('home')
                ->with('msg', 'Coche dado de alta correctamente');
        }

        return redirect()
            ->route('cars.create')
            ->withInput()
            ->with('msg', 'El coche NO pudo ser dado de alta!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Car $car)
    {
        return view('car.show', compact('car'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Car $car)
    {
        return view('car.edit', compact('car'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Car $car)
    {
        $request->validate([
            'plate' => 'required',
            'model' => 'required|integer',
            'brand' => 'required',
            'color' => 'required',
            'image' => 'file|image|mimes:jpeg,jpg,png,gif,webp|max:2048',
        ]);

        $car->plate = $request->plate;
        $car->model = $request->model;
        $car->brand = $request->brand;
        $car->color = $request->color;
        
        if ($request->has('image')) {
            Storage::delete("public/cars/" . $car->image);

            $file = $request->file('image');
            $filename = 'car-' . time() . '.' . $file->getClientOriginalExtension();
            $file->storeAs("public/cars/", $filename);

            $car->image = $filename;
        }
        
        if($car->save()){
            return redirect()
                ->route('home')
                ->with('msg', 'Coche actualizado correctamente');
        }

        return redirect()
            ->route('cars.edit', $car)
            ->withInput()
            ->with('msg', 'El coche NO pudo ser actualizado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Car $car)
    {
        Storage::delete("public/cars/" . $car->image);

        if($car->delete()){
            return redirect()
                ->route('home')
                ->with('msg', 'Coche eliminado correctamente');
        }

        return redirect()
                ->route('home')
                ->with('msg', 'El coche NO eliminado');
    }
}
