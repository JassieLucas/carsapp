<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;
use App\Car;
use Illuminate\Support\Facades\Storage;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Car $car)
    {
        $services = $car->services();
        return view('service.index', compact('services', 'car'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Car $car)
    {
        return view('service.create', compact('car'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Car $car)
    {
        $request->validate([
            'date' => 'required',
            'amount' => 'required|numeric',
            'workshop' => 'required',

        ]);

        $service = new service;
        $service->date = $request->date;
        $service->amount = $request->amount;
        $service->kilometers = $request->kilometers;
        $service->workshop = $request->workshop;
        $service->type = $request->type;
        $service->car_id = $car->id;

        if($request->has('file')){
            $file = $request->file('file');
            $filename = 'service-' . time() . '.' . $file->getClientOriginalExtension();
            $file->storeAs("public/services/", $filename);

            $service->file = $filename;
        }

        if ($service->save()) {
            return redirect()
                ->route('services.index', $car)
                ->with('msg', 'Servicio registrado satisfactoriamente');
        }

        return redirect()
                ->route('services.index', $car)
                ->withInput()
                ->with('msg', 'El servicio NO pudo ser registrado');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Car $car, Service $service)
    {
        return view('service.edit', compact('car', 'service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Car $car, Service $service)
    {
        $request->validate([
            'date' => 'required',
            'amount' => 'required|numeric',
            'workshop' => 'required',

        ]);

        $service->date = $request->date;
        $service->amount = $request->amount;
        $service->kilometers = $request->kilometers;
        $service->workshop = $request->workshop;
        $service->type = $request->type;

        if($request->has('file')){
            Storage::delete('public/services/' . $service->file);

            $file = $request->file('file');
            $filename = 'service-' . time() . '.' . $file->getClientOriginalExtension();
            $file->storeAs("public/services/", $filename);

            $service->file = $filename;
        }

        if ($service->save()) {
            return redirect()
                ->route('services.index', $car)
                ->with('msg', 'Servicio actualizado satisfactoriamente');
        }

        return redirect()
                ->route('services.index', $car)
                ->withInput()
                ->with('msg', 'El servicio NO pudo ser actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Car $car, Service $service)
    {
        Storage::delete("public/services/" . $service->file);

        if($service->delete()){
            return redirect()
                ->route('services.index', $car)
                ->with('msg', 'Servicio eliminado correctamente');
        }

        return redirect()
                ->route('services.index', $car)
                ->with('msg', 'El servicio NO se pudo eliminar');
    }
}
