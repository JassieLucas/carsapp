<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    public function user()
    {
    	return $this->belongsTo(User::class);
    }
    
    public function tenencies()
    {
        return $this->hasMany(Tenency::class)->get();
    }

    public function fines()
    {
        return $this->hasMany(Fine::class)->get();
    }

    public function services()
    {
        return $this->hasMany(Service::class)->get();
    }

    public function documents()
    {
        return $this->hasMany(Document::class)->get();
    }
}
