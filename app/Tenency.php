<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tenency extends Model
{
	protected $dates = [
		'date',
	];
    public function car()
    {
    	return $this->belongsTo(Car::class);
    }

}
